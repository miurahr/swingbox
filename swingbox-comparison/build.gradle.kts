plugins {
    `java-library`
    application
    alias(libs.plugins.spotbugs)
    alias(libs.plugins.spotless)
}

application.applicationName = "comparison"
application.mainClass.set("org.fit.cssbox.swingbox.demo.BrowserComparison")

repositories {
    mavenCentral()
}
dependencies {
    implementation(project.rootProject)
    implementation(libs.cssbox4)
    implementation(libs.apache.httpclient)
    runtimeOnly(libs.slf4j.simple)
}
