plugins {
    `java-library`
    `maven-publish`
    signing
    alias(libs.plugins.git.version)
    alias(libs.plugins.spotbugs)
    alias(libs.plugins.spotless)
}

group = "tokyo.northside"

repositories {
    mavenCentral()
}
dependencies {
    implementation(libs.cssbox4)
    implementation(libs.apache.httpclient)
    implementation(libs.apache.httpclient.cache)
}

publishing {
    publications {
        create<MavenPublication>("mavenJava") {
            from(components["java"])
            pom {
                name.set("swingbox-apache-httpclient")
                group = rootProject.group
                setVersion(rootProject.version)
                description.set("SwingBox")
                url.set("https://codeberg.org/miurahr/swingbox")
                licenses {
                    license {
                        name.set("The GNU Lesser General Public License, Version 3")
                        url.set("https://www.gnu.org/licenses/lgpl-3.html")
                        distribution.set("repo")
                    }
                }
                developers {
                    developer {
                        id.set("miurahr")
                        name.set("Hiroshi Miura")
                        email.set("miurahr@linux.com")
                    }
                }
                scm {
                    connection.set("scm:git:git://codeberg.org/miurahr/swingbox.git")
                    developerConnection.set("scm:git:git://codeberg.org/miurahr/swingbox.git")
                    url.set("https://codeberg.org/miurahr/swingbox")
                }
            }
        }
    }
}

val versionDetails: groovy.lang.Closure<com.palantir.gradle.gitversion.VersionDetails> by extra
val details = versionDetails()
val signKey = listOf("signingKey", "signing.keyId", "signing.gnupg.keyName").find { project.hasProperty(it) }
tasks.withType<Sign> {
    onlyIf { details.isCleanTag && (signKey != null) }
}
signing {
    when (signKey) {
        "signingKey" -> {
            val signingKey: String? by project
            val signingPassword: String? by project
            useInMemoryPgpKeys(signingKey, signingPassword)
        }

        "signing.keyId" -> {/* do nothing */
        }

        "signing.gnupg.keyName" -> {
            useGpgCmd()
        }
    }
    sign(publishing.publications["mavenJava"])
}