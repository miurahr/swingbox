import java.io.FileInputStream
import java.util.Properties

plugins {
    `java-library`
    `maven-publish`
    application
    signing
    alias(libs.plugins.spotbugs)
    alias(libs.plugins.spotless)
    alias(libs.plugins.git.version)
    alias(libs.plugins.nexus.publish)
}

group = "tokyo.northside"
description = "SwingBox"

val dotgit = project.file(".git")
if (dotgit.exists()) {
    apply(plugin = libs.plugins.git.version.get().pluginId)
    val versionDetails: groovy.lang.Closure<com.palantir.gradle.gitversion.VersionDetails> by extra
    val details = versionDetails()
    val baseVersion = details.lastTag.substring(1)
    version = when {
        details.isCleanTag -> baseVersion
        else -> baseVersion + "-" + details.commitDistance + "-" + details.gitHash + "-SNAPSHOT"
    }
} else {
    val gitArchival = project.file(".git-archival.properties")
    val props = Properties()
    props.load(FileInputStream(gitArchival))
    val versionDescribe = props.getProperty("describe")
    val regex = "^v\\d+\\.\\d+\\.\\d+$".toRegex()
    version = when {
        regex.matches(versionDescribe) -> versionDescribe.substring(1)
        else -> versionDescribe.substring(1) + "-SNAPSHOT"
    }
}

val signKey = listOf("signingKey", "signing.keyId", "signing.gnupg.keyName").find { project.hasProperty(it) }
tasks.withType<Sign> {
    onlyIf { signKey != null && !project.version.toString().endsWith("-SNAPSHOT") }
}

val sonatypeUsername: String? by project
val sonatypePassword: String? by project

// ---------- publish to sonatype OSSRH
nexusPublishing {
    repositories.sonatype {
        if (sonatypeUsername != null && sonatypePassword != null) {
            username.set(sonatypeUsername)
            password.set(sonatypePassword)
        } else {
            username.set(System.getenv("SONATYPE_USER"))
            password.set(System.getenv("SONATYPE_PASS"))
        }
    }
}

// -------------- common part: dependencies, code quality assurance
allprojects {
    apply(plugin = "java-library")
    apply(plugin = "com.github.spotbugs")
    apply(plugin = "com.diffplug.spotless")

    repositories {
        mavenCentral()
    }
    java {
        toolchain {
            languageVersion.set(JavaLanguageVersion.of(11))
        }
        withSourcesJar()
        withJavadocJar()
    }
    tasks.withType<Javadoc> {
        setFailOnError(false)
        (options as StandardJavadocDocletOptions).addBooleanOption("Xdoclint:none", true)
        (options as StandardJavadocDocletOptions).addStringOption("Xmaxwarns", "1")
    }
    tasks.withType<JavaCompile>() {
        options.encoding = "UTF-8"
        options.compilerArgs.add("-Xlint:deprecation")
        options.compilerArgs.add("-Xlint:unchecked")
    }
    spotbugs {
        setReportLevel("high")
    }
    spotless {
        java {
            target(listOf("src/*/java/**/*.java"))
            removeUnusedImports()
            palantirJavaFormat()
        }
    }
}

// --------- main library dependency
dependencies {
    implementation(libs.commons.io)
    implementation(libs.commons.codec)
    implementation(libs.jsoup)
    implementation(libs.cssbox4)
    implementation(libs.jstyleparser)
}

// --------- module name
tasks.jar {
    manifest {
        attributes("Automatic-Module-Name" to "tokyo.northside.swigbox")
    }
}

// ---------- main library publishment
publishing {
    publications {
        create<MavenPublication>("mavenJava") {
            from(components["java"])
            pom {
                name.set("swingbox")
                description.set("SwingBox")
                url.set("https://codeberg.org/miurahr/swingbox")
                licenses {
                    license {
                        name.set("The GNU Lesser General Public License, Version 3")
                        url.set("https://www.gnu.org/licenses/lgpl-3.html")
                        distribution.set("repo")
                    }
                }
                developers {
                    developer {
                        id.set("miurahr")
                        name.set("Hiroshi Miura")
                        email.set("miurahr@linux.com")
                    }
                }
                scm {
                    connection.set("scm:git:git://codeberg.org/miurahr/swingbox.git")
                    developerConnection.set("scm:git:git://codeberg.org/miurahr/swingbox.git")
                    url.set("https://codeberg.org/miurahr/swingbox")
                }
            }
        }
    }
}

// ---------- main library signing
signing {
    when (signKey) {
        "signingKey" -> {
            val signingKey: String? by project
            val signingPassword: String? by project
            useInMemoryPgpKeys(signingKey, signingPassword)
        }

        "signing.keyId" -> {/* do nothing */
        }

        "signing.gnupg.keyName" -> {
            useGpgCmd()
        }
    }
    sign(publishing.publications["mavenJava"])
}
