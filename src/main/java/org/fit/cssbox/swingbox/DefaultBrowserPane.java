/*
 * (c) Peter Bielik and Radek Burget, 2011-2012
 * <p>
 * SwingBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * SwingBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with SwingBox. If not, see <http://www.gnu.org/licenses/>.
 */

package org.fit.cssbox.swingbox;

import java.awt.Rectangle;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.Security;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.text.AbstractDocument;
import javax.swing.text.Caret;
import javax.swing.text.DefaultCaret;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import org.fit.cssbox.swingbox.layout.SwingBoxBrowserConfig;
import org.fit.cssbox.swingbox.util.Constants;
import org.fit.cssbox.swingbox.util.GeneralEvent;
import org.fit.cssbox.swingbox.util.GeneralEvent.EventType;

/**
 * ClassicBrowserPane - JEditorPane based component capable to render HTML +
 * CSS with Java 8 URL connection API.
 *
 * @author Peter Bielik
 * @version 1.0
 * @since 1.0 - 28.9.2010
 */
@SuppressWarnings("unused")
public class DefaultBrowserPane extends BrowserPane {
    private InputStream loadingStream;
    private Hashtable<String, Object> pageProperties;

    /**
     * Instantiates a new browser pane.
     */
    public DefaultBrowserPane() {
        super();
    }

    /**
     * Initial settings
     */
    protected void init() {
        // "support for SSL"
        String handlerPkgs = System.getProperty("java.protocol.handler.pkgs");
        if ((handlerPkgs != null) && !(handlerPkgs.isEmpty())) {
            handlerPkgs = handlerPkgs + "|com.sun.net.ssl.internal.www.protocol";
        } else {
            handlerPkgs = "com.sun.net.ssl.internal.www.protocol";
        }
        System.setProperty("java.protocol.handler.pkgs", handlerPkgs);

        final var provider = Security.getProvider("SunPKCS11");
        Security.addProvider(provider);
        // Create custom EditorKit if needed
        if (swingBoxEditorKit == null) {
            swingBoxEditorKit = new SwingBoxEditorKit();
        }

        setEditable(false);
        setContentType("text/html");

        activateTooltip(true);

        Caret caret = getCaret();
        if (caret instanceof DefaultCaret) {
            ((DefaultCaret) caret).setUpdatePolicy(DefaultCaret.NEVER_UPDATE);
        }

        browserConfig = new SwingBoxBrowserConfig();
        browserConfig.registerDocumentSource(getDocumentSourceClass());
    }

    @Override
    protected InputStream getStream(URL page) throws IOException {
        final URLConnection conn = setConnectionProperties(page.openConnection());
        // http://stackoverflow.com/questions/875467/java-client-certificates-over-https-ssl

        if (conn instanceof HttpURLConnection) {
            HttpURLConnection hconn = (HttpURLConnection) conn;
            hconn.setInstanceFollowRedirects(false);
            Object postData = getPostData();
            if (postData != null) {
                handlePostData(hconn, postData);
            }
            int response = hconn.getResponseCode();
            boolean redirect = (response >= 300 && response <= 399);

            /*
             * In the case of a redirect, we want to actually change the URL
             * that was input to the new, redirected URL
             */
            if (redirect) {
                String loc = conn.getHeaderField("Location");
                if (loc.startsWith("http")) {
                    page = new URL(loc);
                } else {
                    page = new URL(page, loc);
                }
                return getStream(page);
            }
        }

        // Connection properties handler should be forced to run on EDT,
        // as it instantiates the EditorKit.
        if (SwingUtilities.isEventDispatchThread()) {
            handleConnectionProperties(conn);
        } else {
            try {
                SwingUtilities.invokeAndWait(() -> handleConnectionProperties(conn));
            } catch (InterruptedException | InvocationTargetException e) {
                throw new RuntimeException(e);
            }
        }
        return conn.getInputStream();
    }

    @Override
    public void setPage(final URL newPage) throws IOException {
        fireGeneralEvent(new GeneralEvent(this, EventType.page_loading_begin, newPage, null));

        if (newPage == null) {
            // TODO fire general event here
            throw new IOException("invalid url");
        }
        final URL oldPage = getPage();
        Object postData = getPostData();

        if ((oldPage == null) || !oldPage.sameFile(newPage) || (postData != null)) {
            // different url or POST method, load the new content

            final InputStream in = getStream(newPage);
            // editor kit is set according to a content type
            EditorKit kit = getEditorKit();

            if (kit == null) {
                UIManager.getLookAndFeel().provideErrorFeedback(this);
            } else {
                document = createDocument(kit, newPage);

                int p = getAsynchronousLoadPriority(document);

                if (p < 0) {
                    // load synchronously
                    loadPage(newPage, oldPage, in, document);
                } else {
                    // load asynchronously
                    Thread t = new Thread(() -> loadPage(newPage, oldPage, in, document));
                    t.setDaemon(true);
                    t.start();
                }
            }
        } else if (oldPage.sameFile(newPage)) {
            if (newPage.getRef() != null) {
                final String reference = newPage.getRef();
                SwingUtilities.invokeLater(() -> scrollToReference(reference));
            }
        }
    }

    private void loadPage(final URL newPage, final URL oldPage, final InputStream in, final Document doc) {
        boolean done = false;
        try {

            synchronized (this) {
                if (loadingStream != null) {
                    // we are loading asynchronously, so we need to cancel
                    // the old stream.
                    loadingStream.close();
                    loadingStream = null;
                }

                loadingStream = in;
            }

            // read the content
            read(loadingStream, doc);
            // set the document to the component
            setDocument(doc);

            final String reference = newPage.getRef();
            // Have to scroll after painted.
            SwingUtilities.invokeLater(() -> {
                // top of the pane
                scrollRectToVisible(new Rectangle(0, 0, 1, 1));
                if (reference != null) {
                    scrollToReference(reference);
                }
            });

            done = true;

        } catch (IOException ioe) {
            UIManager.getLookAndFeel().provideErrorFeedback(this);
        } finally {
            synchronized (this) {
                if (loadingStream != null) {
                    try {
                        loadingStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                loadingStream = null;
            }

            if (done) {
                SwingUtilities.invokeLater(() -> firePropertyChange("page", oldPage, newPage));
            }
        }
    }

    private Document createDocument(EditorKit kit, URL page) {
        // we have pageProperties, because we can be in situation that
        // old page is being removed & new page is not yet created...
        // we need somewhere to store important data.
        Document doc = kit.createDefaultDocument();
        if (pageProperties != null) {
            // transfer properties discovered in stream to the
            // document property collection.
            for (Enumeration<String> e = pageProperties.keys(); e.hasMoreElements(); ) {
                Object key = e.nextElement();
                doc.putProperty(key, pageProperties.get(key));
            }
        }
        if (doc.getProperty(Document.StreamDescriptionProperty) == null) {
            doc.putProperty(Document.StreamDescriptionProperty, page);
        }
        return doc;
    }

    private int getAsynchronousLoadPriority(Document doc) {
        return (doc instanceof AbstractDocument ? ((AbstractDocument) doc).getAsynchronousLoadPriority() : -1);
    }

    protected Object getPostData() {
        return getDocument().getProperty(Constants.PostDataProperty);
    }

    /**
     * Handle URL connection properties (most notably, content type).
     */
    private void handleConnectionProperties(URLConnection conn) {
        if (pageProperties == null) {
            pageProperties = new Hashtable<>(22);
        }

        String type = conn.getContentType();
        if (type != null) {
            setContentType(type);
        }

        pageProperties.put(Document.StreamDescriptionProperty, conn.getURL());

        Map<String, List<String>> header = conn.getHeaderFields();

        Set<String> keys = header.keySet();
        Object obj;
        for (String key : keys) {
            obj = header.get(key);
            if (key != null && obj != null) {
                pageProperties.put(key, obj);
            }
        }
    }

    /*
     * An unofficial user agent format, based on the above, used by Web browsers
     * is as follows: Mozilla/[version] ([system and browser information])
     * [platform] ([platform details]) [extensions]. For example, Safari on
     * the iPad has used the following: Mozilla/5.0 (iPad; U; CPU OS 3_2_1
     * like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko)
     * Mobile/7B405.
     */
    private URLConnection setConnectionProperties(URLConnection conn) {
        // http://www.useragentstring.com/index.php
        // http://tools.ietf.org/html/rfc1945
        // Opera 11.50 : Opera/9.80 (X11; Linux i686; U; sk) Presto/2.9.168
        // Version/11.50
        // CSSBox : Mozilla/5.0 (compatible; BoxBrowserTest/2.x; Linux)
        // CSSBox/2.x (like Gecko)
        // FireFox : Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9b5)
        // Gecko/2008032620 Firefox/3.0b5
        // IE8 : Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0;
        // .NET CLR 2.0.50727; .NET CLR 1.1.4322; .NET CLR 3.0.04506.30; .NET
        // CLR 3.0.04506.648)
        // SwingBox : Mozilla/5.0 (compatible; SwingBox/1.x; Linux; U)
        // CSSBox/2.x (like Gecko)

        conn.setRequestProperty(
                "User-Agent", "Mozilla/5.0 (compatible; SwingBox/1.x; Linux; U) CSSBox/4.x (like Gecko)");
        conn.setRequestProperty("Accept-Charset", "utf-8");

        return conn;
    }

    private void handlePostData(HttpURLConnection conn, Object postData) throws IOException {
        conn.setDoOutput(true);
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        try (final var os = new DataOutputStream(conn.getOutputStream())) {
            os.writeBytes((String) postData);
        }
    }
}
