/*
 * (c) Peter Bielik and Radek Burget, 2011-2012
 * Copyright 2020 White Magic Software, Ltd.
 *
 * SwingBox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SwingBox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with SwingBox. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.fit.cssbox.swingbox.util;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.net.URI;
import java.net.URL;
import org.apache.commons.codec.CharEncoding;
import org.fit.cssbox.css.CSSNorm;
import org.fit.cssbox.css.DOMAnalyzer;
import org.fit.cssbox.io.DefaultDocumentSource;
import org.fit.cssbox.io.DocumentSource;
import org.fit.cssbox.layout.BrowserCanvas;
import org.fit.cssbox.layout.BrowserConfig;
import org.fit.cssbox.layout.Viewport;
import org.jsoup.Jsoup;
import org.jsoup.helper.W3CDom;
import org.jsoup.nodes.Document;
import org.w3c.dom.NodeList;

/**
 * This is customizable default implementation of CSSBoxAnalyzer.
 *
 * @author Peter Bielik
 * @author Radek Burget
 */
@SuppressWarnings("unused")
public class DefaultAnalyzer implements CSSBoxAnalyzer {
    protected final W3CDom mDom = new W3CDom();

    protected org.w3c.dom.Document w3cdoc;
    protected BrowserCanvas canvas;
    protected BrowserConfig mBrowserConfig;

    public DefaultAnalyzer() {
        mBrowserConfig = new BrowserConfig();
        mBrowserConfig.registerDocumentSource(getDocumentSourceClass());
        mBrowserConfig.setLoadImages(true);
        mBrowserConfig.setLoadBackgroundImages(true);
        mBrowserConfig.setLoadFonts(true);
    }

    @SuppressWarnings("unchecked")
    private Class<? extends DocumentSource> getDocumentSourceClass() {
        String cname = System.getProperty(Constants.DEFAULT_DOCUMENT_SOURCE_PROPERTY, Constants.PROPERTY_NOT_SET);
        if (!cname.equals(Constants.PROPERTY_NOT_SET)) {
            try {
                Class<? extends DocumentSource> c;
                ClassLoader loader = getClass().getClassLoader();
                if (loader != null) {
                    c = (Class<? extends DocumentSource>) loader.loadClass(cname);
                } else {
                    c = (Class<? extends DocumentSource>) Class.forName(cname);
                }
                return c;
            } catch (Exception ignored) {
            }
        }
        // use default.
        return DefaultDocumentSource.class;
    }

    @Override
    public Viewport analyze(final DocumentSource docSource, final Dimension dim) throws Exception {
        final URL url = docSource.getURL();
        final URI uri = url.toURI();

        final Document document = Jsoup.parse(docSource.getInputStream(), CharEncoding.UTF_8, uri.toString());
        docSource.close();

        w3cdoc = mDom.fromJsoup(document);

        // Create the CSS analyzer
        final DOMAnalyzer da = new DOMAnalyzer(w3cdoc, url);
        da.attributesToStyles();
        da.addStyleSheet(null, CSSNorm.stdStyleSheet(), DOMAnalyzer.Origin.AGENT);
        da.addStyleSheet(null, CSSNorm.userStyleSheet(), DOMAnalyzer.Origin.AGENT);
        da.getStyleSheets();

        final BufferedImage image = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
        canvas = new BrowserCanvas(da.getRoot(), da, url);
        canvas.setConfig(mBrowserConfig);
        canvas.setImage(image);
        canvas.createLayout(dim);

        return canvas.getViewport();
    }

    @Override
    public Viewport update(Dimension dim) {
        canvas.createLayout(dim);
        return canvas.getViewport();
    }

    @Override
    public org.w3c.dom.Document getDocument() {
        return w3cdoc;
    }

    @Override
    public String getDocumentTitle() {
        final NodeList titles = w3cdoc.getElementsByTagName("title");

        if (titles.getLength() > 0) {
            return titles.item(0).getTextContent();
        }

        return "";
    }
}
