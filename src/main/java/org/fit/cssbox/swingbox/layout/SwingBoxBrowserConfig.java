package org.fit.cssbox.swingbox.layout;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import org.fit.cssbox.io.DocumentSource;
import org.fit.cssbox.layout.BrowserConfig;

public class SwingBoxBrowserConfig extends BrowserConfig {

    public SwingBoxBrowserConfig() {
        super();
    }

    public DocumentSource createDocumentSource(URL page, Object postData) {
        try {
            Constructor<? extends DocumentSource> constr =
                    getDocumentSourceClass().getConstructor(URL.class, Object.class);
            return constr.newInstance(page, postData);
        } catch (NoSuchMethodException
                | InstantiationException
                | IllegalAccessException
                | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }
}
