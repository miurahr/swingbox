package org.fit.cssbox.swingbox.httpclient;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import org.fit.cssbox.io.DocumentSource;

public class Java11DocumentSource extends DocumentSource {

    private HttpRequest httpRequest;
    private String mContentType = "";
    private InputStream mInputStream;

    public Java11DocumentSource(URL url) throws IOException {
        super(url);
        httpRequest = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(url.toString()))
                .timeout(Duration.ofSeconds(15))
                .build();
    }

    public Java11DocumentSource(URL base, String urlstring) throws IOException {
        super(base, urlstring);
        httpRequest = HttpRequest.newBuilder(URI.create(urlstring)).build();
    }

    @SuppressWarnings("uncheck")
    public Java11DocumentSource(URL url, Object postData) throws IOException {
        this(url);
        httpRequest = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString((String) postData))
                .header("Content-Type", "application/x-www-form-urlencoded")
                .uri(URI.create(url.toString()))
                .build();
    }

    public Java11DocumentSource setURI(URI uri) {
        httpRequest = HttpRequest.newBuilder().uri(uri).build();
        return this;
    }

    @Override
    public URL getURL() {
        try {
            return httpRequest.uri().toURL();
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getContentType() {
        return mContentType;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        HttpResponse<InputStream> response;
        try {
            HttpClient httpClient = HttpClient.newBuilder()
                    .followRedirects(HttpClient.Redirect.ALWAYS)
                    .build();
            response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofInputStream());
            int statusCode = response.statusCode();
            if (statusCode != 200) {
                throw new IOException(String.format("Got http error %d", statusCode));
            }
            mContentType = response.headers().map().get("Content-Type").get(0);
            mInputStream = response.body();
            return mInputStream;
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() throws IOException {
        if (mInputStream != null) {
            mInputStream.close();
        }
    }
}
