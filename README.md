# SwingBox

This is a fork of SwingBox project.

SwingBox is a Java Swing component that allows displaying the (X)HTML documents including the CSS support.
It is designed as a JEditorPane replacement with considerably better rendering results.
SwingBox is written by pure Java, and it is using the CSSBox rendering engine for rendering the documents.
	
## Changes

### swingbox-apache-httpclient

Performance CSSBoxAnalyzer, introduced in 1.1.2, become a transport addon module.
 

### Original authors

- SwingBox was originated by Peter Bielik, and was maintained by Radek Burget.
  https://github.com/radkovo/SwingBox

- See the original project page for more information.
[http://cssbox.sourceforge.net/swingbox](http://cssbox.sourceforge.net/swingbox)
