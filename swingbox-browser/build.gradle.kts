plugins {
    `java-library`
    application
    alias(libs.plugins.spotbugs)
    alias(libs.plugins.spotless)
}

application.applicationName = "browser"
application.mainClass.set("org.fit.cssbox.swingbox.demo.SwingBrowser")

repositories {
    mavenCentral()
}
dependencies {
    implementation(project.rootProject)
    implementation(libs.jstyleparser)
    implementation("com.beust:jcommander:1.82")
    implementation(project(":swingbox-apache-httpclient"))
    runtimeOnly(libs.slf4j.simple)
}