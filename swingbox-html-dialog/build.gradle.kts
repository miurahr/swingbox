plugins {
    `java-library`
    application
    alias(libs.plugins.spotbugs)
    alias(libs.plugins.spotless)
}

application.applicationName = "html-dialog"
application.mainClass.set("tokyo.northside.swingbox.demo.HtmlDialog")

repositories {
    mavenCentral()
}
dependencies {
    implementation(project.rootProject)
    runtimeOnly(libs.slf4j.simple)
}

