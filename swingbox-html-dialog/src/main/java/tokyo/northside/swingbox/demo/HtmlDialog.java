package tokyo.northside.swingbox.demo;

import java.awt.Component;
import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import org.fit.cssbox.swingbox.BrowserPane;

public final class HtmlDialog extends JDialog {
    BrowserPane swingbox;

    public HtmlDialog() {
        try {
            initComponents();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void initComponents() throws IOException {
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setTitle("\"HTML dialog\" Demo Application");
        JPanel panel = new JPanel();
        panel.setPreferredSize(new Dimension(400, 350));
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(createSwingBox());
        JButton button = new JButton("OK");
        panel.add(button);
        getContentPane().add(panel);
        button.addActionListener(actionEvent -> dispose());
        pack();
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private Component createSwingBox() throws IOException {
        swingbox = new BrowserPane();
        Path file = Paths.get(installDir(), "docs", "readme.html");
        URL page = file.toUri().toURL();
        swingbox.setPage(page);
        swingbox.setPreferredSize(new Dimension(400, 300));
        return swingbox;
    }

    /** Caching install dir */
    private static String installDir = null;

    /**
     * Returns installation directory.
     */
    public static String installDir() {
        if (installDir == null) {
            File file = null;
            try {
                URI sourceUri = HtmlDialog.class
                        .getProtectionDomain()
                        .getCodeSource()
                        .getLocation()
                        .toURI();
                if (sourceUri.getScheme().equals("file")) {
                    File uriFile = Paths.get(sourceUri).toFile();
                    // If running from a JAR, get the enclosing folder
                    // (the JAR is assumed to be at the installation root)
                    if (uriFile.getName().endsWith(".jar")) {
                        file = uriFile.getParentFile();
                    }
                }
            } catch (URISyntaxException ignored) {
            }
            // If running from an IDE, or build tool;
            // use "$CWD".
            if (file == null) {
                file = Paths.get(".").toFile();
            }
            installDir = file.getAbsolutePath();
        }
        return installDir;
    }

    /**
     * The main method allows us to run as a standalone demo.
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(HtmlDialog::new);
    }
}
